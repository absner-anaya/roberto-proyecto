/**
 * TipoExamenController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

    create: function(req,res) {
        Examen.create(req.body).exec((error, data) => {
            if (error) {
                return res.status(500).json({
                    code: 500,
                    message: 'Error almacenando datos',
                    content: error
                });
            } else {
                return res.status(200).json({
                    code: 200,
                    message: 'Data almacenada exitosamente',
                    content: data
                });
            }
        })
    },
    update: function(req,res) {
        Examen.update({id: req.params.id}, req.body).exec((error, examen)  =>  {
            if (error) {
                return  res.status(500).send({
                    code:500,
                    message: 'Error al actualizar datos',
                    content: error
                })
            } else {
                return res.status(200).send({
                    code: 200,
                    message: 'Datos actualizados correctamente',
                    content: examen
                })
            }
        })
    },
    destroy: function (req,res){
        
        Examen.destroy({id: req.params.id}).exec(function (err, examen) {
            if(err){
                return res.status(500).send({
                    code: 500,
                    message: 'Error al eliminar datos',
                    content: err
                });
            }else{
                return res.status(200).send({
                    code: 200,
                    message: 'user delete'
                });
            }
        })
    },
    find: function(req,res) {
        
        Examen.find().exec((error, examen)    =>  {
            if (error) {
                return res.status(500).send({
                    code: 500,
                    message: 'Error al obtener datos',
                    content: error
                })
            } else {
                return res.status(200).send({
                    code: 200,
                    message: 'Datos obtenidos exitosamente',
                    content: examen
                })
            }
        })
    },
    findOne: function(req,res) {
        Examen.findOne({id: req.params.id}).exec((error, examen)  =>  {
            if (error) {
                return res.status(500).send({
                    code: 500,
                    message: 'Error al obtener datos',
                    content: error
                })
            } else {
                return res.status(200).send({
                    code: 200,
                    message: 'Datos obtenidos exitosamente',
                    content: examen
                })
            }
        })
    }
  

};

