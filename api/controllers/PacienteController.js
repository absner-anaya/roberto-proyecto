/**
 * PacienteController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

    create: function(req,res) {
        Paciente.create(req.body).exec((error, data) => {
            if (error) {
                return res.status(500).json({
                    code: 500,
                    message: 'Error almacenando datos',
                    content: error
                });
            } else {
                return res.status(200).json({
                    code: 200,
                    message: 'Data almacenada exitosamente',
                    content: data
                });
            }
        })
    },
    update: function(req,res) {
        Paciente.update({id: req.params.id}, req.body).exec((error, paciente)  =>  {
            if (error) {
                return  res.status(500).send({
                    code:500,
                    message: 'Error al actualizar datos',
                    content: error
                })
            } else {
                return res.status(200).send({
                    code: 200,
                    message: 'Datos actualizados correctamente',
                    content: paciente
                })
            }
        })
    },
    destroy: function (req, res) {
        Paciente.destroy({id: req.params.id}).exec(function (err, paciente) {
            if(err){
                return res.status(500).send({
                    code: 500,
                    message: 'Error al eliminar datos',
                    content: paciente
                });
            }else{
                return res.status(200).send({
                    code: 200,
                    message: 'user delete',
                    content: paciente
                });
            }
        });
    },
    find: function(req,res) {
        Paciente.find().exec((error, cpaciente)    =>  {
            if (error) {
                return res.status(500).send({
                    code: 500,
                    message: 'Error al obtener datos',
                    content: error
                })
            } else {
                return res.status(200).send({
                    code: 200,
                    message: 'Datos obtenidos exitosamente',
                    content: cpaciente
                })
            }
        })
    },
    findOne: function(req,res) {
        Paciente.findOne({cedula: req.params.id}).exec((error, paciente)  =>  {
            if (error) {
                return res.status(500).send({
                    code: 500,
                    message: 'Error al obtener datos',
                    content: error
                })
            } else {
                return res.status(200).send({
                    code: 200,
                    message: 'Datos obtenidos exitosamente',
                    content: paciente
                })
            }
        })
    }


};

