/**
 * HistorialController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

    create: function(req,res) {
        Historial.create(req.body).exec((error, data) => {
            if (error) {
                return res.status(500).json({
                    code: 500,
                    message: 'Error almacenando datos',
                    content: error
                });
            } else {
                return res.status(200).json({
                    code: 200,
                    message: 'Data almacenada exitosamente',
                    content: data
                });
            }
        })
    },
    update: function(req,res) {
        Historial.update({id: req.params.id}, req.body).exec((error, historia)  =>  {
            if (error) {
                return  res.status(500).send({
                    code:500,
                    message: 'Error al actualizar datos',
                    content: error
                })
            } else {
                return res.status(200).send({
                    code: 200,
                    message: 'Datos actualizados correctamente',
                    content: historia
                })
            }
        })
    },
    destroy: function (req, res) {
        Historial.destroy({id: req.params.id}).exec(function (err, historia) {
            if(err){
                return res.status(500).send({
                    code: 500,
                    message: 'Error al eliminar datos'
                });
            }else{
                return res.status(200).send({
                    code: 200,
                    message: 'user delete',
                    content: historia
                });
            }
        });
    },
    find: function(req,res) {
        Historial.find().populateAll().exec((error, chistoria)    =>  {
            if (error) {
                return res.status(500).send({
                    code: 500,
                    message: 'Error al obtener datos',
                    content: error
                })
            } else {
                return res.status(200).send({
                    code: 200,
                    message: 'Datos obtenidos exitosamente',
                    content: chistoria
                })
            }
        })
    },
    findOne: function(req,res) {
        Historial.findOne({id: req.params.id}).populateAll().exec((error, historia)  =>  {
            if (error) {
                return res.status(500).send({
                    code: 500,
                    message: 'Error al obtener datos',
                    content: error
                })
            } else {
                return res.status(200).send({
                    code: 200,
                    message: 'Datos obtenidos exitosamente',
                    content: historia
                })
            }
        })
    }
  

};

