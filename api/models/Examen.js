/**
 * TipoExamen.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    tipo_examen: {
      type: 'string'
    },
    precio_examen: {
      type: 'number',
      columnType: 'float'
    },
    descripcion: {
      type: 'string'
    }

  },

};

